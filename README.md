# Circuits

+ LC3
  - A circuit that simulates a simplified version of LC3
  - Phases and instructions are controlled by the Control Unit
  - Main controlls the flow of data between all of the internal circuits
     - Uses the InstructionDifferentiator to determine when to switch the data flow to allow load and store to function
  - ALU does all of the calculating for the operations
	- Does basic operations and does the math for the ld and st operations and variants
  - Registers manages incoming and outgoing data to the registers
  - Helper circuit called InstructionDifferentiator just figures out which type of instruction the instruction is

+ Test
  - 0000 1025 3000 0000 2400 c040
    - Tests AND, ST, LD, and JMP instructions
      - Expectations: 
        - the first register should hold 5 after the second store phase
          - on error, double check the internal values of the alu for unexpected numbers and check paths to and from the alu for unexpected numbers.
        - the memory location after 3000 should become 5 after the 3rd store phase
          - on error, double check the logic under the alu for errors, also check the alu for the right location in ram to store 5. Also could be a register issue, make sure the output is 5 in store phase.
        - the third register(labeled c) should be filled with c040 after the 5th store phase
          - if this isn't the case, on the 5th store phase, check that RamWe is enabled, that the highlighted memory unit is c040, and that the data going into the registers circuit reads as c040.
        - the pc should change to 0 after the 6th store phase
          - check the instruction section of the control unit if this isn't the case
